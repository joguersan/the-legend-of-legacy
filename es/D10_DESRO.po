msgid ""
msgstr ""
"Project-Id-Version: Legend of Legacy\n"
"Report-Msgid-Bugs-To: transin\n"
"POT-Creation-Date: 2/09/21\n"
"PO-Revision-Date: 2022-03-19 13:23+0000\n"
"Last-Translator: Jorge Guerra <joguersan@gmail.com>\n"
"Language-Team: Spanish <https://weblate.tradusquare.es/projects/"
"the-legend-of-legacy/d10_desro/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.11\n"

#. Dialog
msgctxt "0"
msgid ""
"Hm... no one's home?\n"
"Let's explore a bit!"
msgstr ""
"Hm... ¿no hay nadie en\n"
"casa? ¡Vamos a explorar!"

#. Empty
msgctxt "1"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "2"
msgid ""
"I doubt it's empty...\n"
"Let's be careful here."
msgstr ""
"Dudo que esté vacía...\n"
"Id con cuidado."

#. Empty
msgctxt "3"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "4"
msgid ""
"I doubt we're alone.\n"
"...Let's be careful."
msgstr ""
"Dudo que estemos solos.\n"
"Vayamos con cuidado..."

#. Empty
msgctxt "5"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "6"
msgid ""
"I don't think we're alone.\n"
"...Let's be careful here."
msgstr ""
"No creo que estemos solos.\n"
"Vayamos con cuidado..."

#. Empty
msgctxt "7"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "8"
msgid ""
"No god-people here...\n"
"But something else\n"
"might be here instead!"
msgstr ""
"No hay celestiales...\n"
"¡Pero podría haber\n"
"algo mucho peor!"

#. Empty
msgctxt "9"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "10"
msgid ""
"Bugs... bugs... bugs...\n"
"Can we turn around?"
msgstr ""
"Bichos... bichos... bichos...\n"
"¿Podemos dar media vuelta?"

#. Empty
msgctxt "11"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "12"
msgid "Ah, the home of the Wasp Clan!"
msgstr "¡Ah, el hogar del clan Avispón!"

#. Empty
msgctxt "13"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "14"
msgid "How about we DON'T be dinner!"
msgstr ""
"¡¿Qué tal si no acabamos\n"
"siendo su cena?!"

#. Empty
msgctxt "15"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "16"
msgid "Let's not become their supper."
msgstr ""
"No me gustaría acabar\n"
"siendo su cena."

#. Empty
msgctxt "17"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "18"
msgid "Let's not become their dinner!"
msgstr ""
"¡Vamos a intentar\n"
"no acabar siendo su cena!"

#. Empty
msgctxt "19"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "20"
msgid "I don't want to be their dinner!"
msgstr "¡No me da la gana ser su cena!"

#. Empty
msgctxt "21"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "22"
msgid "Let's not be their supper."
msgstr ""
"No me apetece acabar\n"
"siendo su cena."

#. Empty
msgctxt "23"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "24"
msgid "I've never liked bugs..."
msgstr "Qué asco de bichos..."

#. Empty
msgctxt "25"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "26"
msgid ""
"And if THAT'S their queen,\n"
"well, I like her least of all!"
msgstr ""
"¡Y como esa cosa sea su\n"
"reina, asco se queda corto!"

#. Empty
msgctxt "27"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "28"
msgid "The queen of the Wasp Clan..."
msgstr "La reina del clan Avispón..."

#. Empty
msgctxt "29"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "30"
msgid "Let us do battle!"
msgstr "¡Es hora de luchar!"

#. Empty
msgctxt "31"
msgid "<!empty>"
msgstr "<!empty>"
