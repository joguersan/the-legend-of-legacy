��    \      �     �      �     �     �     �          )     9  #   L  G   p     �  "   �  &   �  =   	  ,   V	  +   �	  -   �	  (   �	  (   
  (   /
  *   X
  &   �
  +   �
     �
  )   �
  "      '   C     k  B   �     �     �     �  !        .     M     l     �     �     �     �     �     �               (     7     H     Y     r     �     �     �     �     �     �     �          "     3  S   G     �     �     �     �     �     �     	       &   ,  9   S  >   �  -   �  9   �  .   4  9   c  4   �  7   �  3   
  +   >  0   j  %   �  S   �  B     h   X     �     �     �          /     O     g     w  ?   �    �     �     �     �  
   �     �     �       ?         `     l     �  3   �  "   �  #   �  !         =      ^       $   �  1   �  )   �        "   3     V  )   s     �  F   �     �                    3     E     X     k     x     �     �     �     �     �     �     �  
   �     �     �     �     �  	   �  	   �            	        %     1     7  	   >  M   H     �     �     �     �     �     �     �     �  )   �  /   �  <   +  *   h  1   �  )   �  1   �  (   !  0   J  !   {     �     �      �  N   �  C   G  Z   �     �     �          #     9     ?     Q     X  7   `         T                                    +      M               N       /   C      W   X   ;               4   6   
           U       G   J   1   &      %   <          >          *   ?   L   (   B          P   #      V       [   -          D      @       =   2   9   A             Y   "   F       $   ,       8   O   Z   I   .      !   S              K      0   E      H   Q           :   7                        R      \   	       )      5                  '           3    0x0010BAD8Leave 0x0010BAE0Travel 0x0010BBE0Leave Map 0x0010BBF8Enter the %s 0x0010BC08Talk 0x0010BC10Examine 0x00114E30Select a main character. 0x00115DA4Screenshot saved.
* It can be viewed in Nintendo 3DS Camera. 0x00126D4CFormations 0x0012A928The party has fallen... 0x0012A940{color1}%s{color4} fainted. 0x0012A950{color1}%s{color4} and {color1}%s{color4} fainted. 0x0012B120{color1}Attack{color4} increased! 0x0012B134{color1}Guard{color4} increased! 0x0012B148{color1}Support{color4} increased! 0x0012B160{color1}HP{color4} increased! 0x0012B170{color1}SP{color4} increased! 0x0012BAA4{color1}%s{color4} increased! 0x0012BABCLearned {color1}%s: %s{color4}! 0x0012DCCCSD Card is write-protected. 0x0012DD04Too many screenshots on SD Card. 0x0012DD28No SD Card inserted. 0x0012DD40Insufficient space on SD Card. 0x0012DD60Failed to read SD Card. 0x0012DD78SD Card communication error. 0x0013FFA4Formations 0x00141FECYou have no contract with this element.
Use this charm? 0x0015BDA8Screen Capture 0x001F55A20BFormations 0x001F574CElementals 0x001F5914Choose your formation. 0x001F592CChoose your action. 0x001F5940Choose your target. 0x00205AE8Story Cleared 0x00262994Star Graal 0x00262D84Orichalcon 0x0026BCECHire a trading ship? 0x0030DE88Fire  0x0030DE90Water  0x0030DEA0Shadow  0x0030FE5CNo 0x0030FE60Give Up 0x0030FE68Yes 0x0030FE6CRetry 0x0030FF40Guard 0x0030FF48Counterstrike 0x0030FF58Pressure 0x0030FF64Anticipate 0x0030FF70Rush 0x0030FF78Ambush 0x0030FF80Attack 0x0030FF88Caution 0x0030FF90Recklessness 0x0030FFA0Suppress 0x0030FFACSupport 0x0030FFB4Delay 0x0030FFBCRecovery 0x00310008FAILED TO SAVE!
Cannot quick save here.
Please try a different location. 0x00310F58Caravel 0x00310F60Galleon 0x0032CEC9S-Sword 0x0032CED1L-Sword 0x0032CEDDSpear 0x0032CEE3Staff 0x0032CEEDShield 0x0032CEF4Water 0x0032D4B4How didst thou get in here? 0x0032D4EBThou hast no warrant
to defile this sanctuary. 0x0032D51AWhy didst the elementals
bar not thy entry? Begone. 0x0032D54EI am the wind.
Thou shalt know me. 0x0032D571Begone, corruptor.
Taint this land no further. 0x0032D5A2I am the flame.
Thou shalt know me. 0x0032D5C6Begone, tormentor.
Curse this land no further. 0x0032D5F5Th-Thou art plague.
Thou art c-contagion. 0x0032D61FThou art ruin... Ruin...
Ruin. Ruin... Ruin. 0x0032D64CThou dost belong not
within these halls. 0x0032D690I shall dispose
of this garbage. 0x0032D6B1Thou art creatures
of no consequence. 0x0032D6D7Death be thy
dominion now. 0x0032DBC1Forged a {color1}contract{color4} with the {color1}elementals{color4}... 0x0032E119{color1}%s{color4} learned a new {color1}charm{color4}! 0x0032E378Try using the power of the {color1}charm{color4} inside the {color1}Whispering Shard{color4}. 0x0032E864Water Elementals 0x0032E875Air Elementals 0x0032E884Fire Elementals 0x0032E894Shadow Elementals 0x0032E8A6Enemies/Allies (All) 0x0032E8BBAllies (All) 0x0032EC76Sell 0x0032EC7BBuy 0x0032EC90Tales of the {color2}$_N{color4}
 circulate Initium! Project-Id-Version: The Legends of Legacy
Report-Msgid-Bugs-To: tradusquare@gmail.com
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.1
 Salir Viajar Salir Entrar: %s Hablar Observar Elige al personaje principal. Captura guardada.
* Puedes verla en la Cámara de Nintendo 3DS. Formaciones El grupo ha caído... {color1}%s{color4} ha caído. {color1}%s{color4} y {color1}%s{color4} han caído. ¡{color1}Ataque{color4} mejorado! ¡{color1}Guardia{color4} mejorada! ¡{color1}Apoyo{color4} mejorado! ¡{color1}PS{color4} aumentados! ¡{color1}PH{color4} aumentados! ¡Mejorado: {color1}%s{color4}! ¡Aprendido: {color1}%s: %s{color4}! La tarjeta SD está protegida frente a escritura. Hay demasiadas capturas en la tarjeta SD. No hay tarjeta SD. No queda espacio en la tarjeta SD. Error al leer la tarjeta SD. Error de comunicación con la tarjeta SD. Formaciones ¿Quieres usar este hechizo?
No has forjado un pacto con el elemental. Captura de pantalla 0BFormaciones Elementales Elige la formación. Elige la acción. Elige el objetivo. Partida completada Grial Astral Oricalco ¿Quieres enviar un navío? Fuego Agua Sombra No Rendirse Sí Reintentar Guardia Contraataque Presión Antelación Acometida Emboscada Ataque Precaución Temeridad Contención Apoyo Demora Curación ¡ERROR AL GUARDAR!
Guardado rápido no disponible,
inténtalo en otro sitio. Carabela Galeón Espada Espadón Lanza Bastón Escudo Agua ¿Cómo habéis arribado
a aqueste lugar? No habéis razón para
profanar este santuario. ¿Los elementales no preveyeron
vueso advenimiento? Marchad. Yo soy el viento.
Como tal me conoceréis. Marchad, corruptores.
Esta tierra no macularéis. Yo soy la llama.
Como tal me conoceréis. Marchad, corruptores.
Esta tierra no macularéis. S-sois una plaga.
Sois una infestación. Sois la ruina... Ruina...
Ruina. Ruina... Ruina. No pertenecéis
a estas cámaras. Me desharé de
la basura. Sois criaturas
insignificantes. La muerte será
vuestro dominio. Has forjado un {color1}contrato{color4} con los {color1}elementales{color4}... ¡{color1}%s{color4} ha aprendido un nuevo {color1}hechizo{color4}! Extrae los {color1}hechizos{color4} del interior del {color1}fragmento susurrante{color4}. Elementales de agua Elementales de aire Elementales de fuego Elementales sombríos Todos Todos los aliados Vender Comprar ¡{color2}$_N{color4} es la comidilla
 de todo Initium! 