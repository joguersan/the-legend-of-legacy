msgid ""
msgstr ""
"Project-Id-Version: Legend of Legacy\n"
"Report-Msgid-Bugs-To: transin\n"
"POT-Creation-Date: 2/09/21\n"
"PO-Revision-Date: 2022-01-28 21:40+0000\n"
"Last-Translator: Jorge Guerra <joguersan@gmail.com>\n"
"Language-Team: Spanish <https://weblate.tradusquare.es/projects/"
"the-legend-of-legacy/d10_desgr/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9\n"

#. Dialog
msgctxt "0"
msgid ""
"They're just like you and me.\n"
"Except they have scales...\n"
"and they're ugly."
msgstr ""
"Ellos son como tú y como yo.\n"
"Salvo porque tienen escamas...\n"
"y son horrendos."

#. Empty
msgctxt "1"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "2"
msgid ""
"The monsters here\n"
"are scaled humanoids..."
msgstr ""
"Estos monstruos son\n"
"humanoides con escamas..."

#. Empty
msgctxt "3"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "4"
msgid ""
"Scaled guys, huh?\n"
"Be ready for anything."
msgstr ""
"Tipos escamosos, ¿eh?\n"
"No bajéis la guardia."

#. Empty
msgctxt "5"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "6"
msgid "They're so... scaley...!"
msgstr ""
"La verdad es que...\n"
"¡me escaman!"

#. Empty
msgctxt "7"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "8"
msgid ""
"Creatures with scales\n"
"do not frighten me!"
msgstr ""
"¡No me asustan unas\n"
"criaturas con escamas!"

#. Empty
msgctxt "9"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "10"
msgid ""
"Looks like we're going\n"
"up against some scaley\n"
"creatures this time."
msgstr ""
"Parece que esta vez nos\n"
"toca luchar contra\n"
"criaturas escamosas."

#. Empty
msgctxt "11"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "12"
msgid ""
"The monsters here\n"
"are scaley humanoids..."
msgstr ""
"Estos monstruos son\n"
"humanoides con escamas..."

#. Empty
msgctxt "13"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "14"
msgid ""
"Be careful of their\n"
"magic spells!"
msgstr "¡Cuidado con sus hechizos!"

#. Empty
msgctxt "15"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "16"
msgid ""
"We're about to go up\n"
"against a spellcaster..."
msgstr ""
"Vamos a luchar contra\n"
"un mago..."

#. Empty
msgctxt "17"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "18"
msgid ""
"I hate magic. Nobody\n"
"fights fair anymore!"
msgstr ""
"Odio la magia. ¡¿Es que\n"
"ya nadie lucha limpiamente?!"

#. Empty
msgctxt "19"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "20"
msgid ""
"Be careful of their\n"
"magic spells!"
msgstr "¡Cuidado con su magia!"

#. Empty
msgctxt "21"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "22"
msgid ""
"We're about to go up\n"
"against a spellcaster..."
msgstr ""
"Vamos a luchar\n"
"contra un mago..."

#. Empty
msgctxt "23"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "24"
msgid ""
"Be careful of their\n"
"magic spells!"
msgstr "¡Cuidado con sus hechizos!"

#. Empty
msgctxt "25"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "26"
msgid ""
"This is a clan I had not\n"
"previously been aware of."
msgstr ""
"No tenía conocimiento\n"
"de que existiera este clan."

#. Empty
msgctxt "27"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "28"
msgid ""
"It seems a lot has changed\n"
"during my hibernation!"
msgstr ""
"¡Parece que me he perdido\n"
"muchas cosas mientras\n"
"hibernaba!"

#. Empty
msgctxt "29"
msgid "<!empty>"
msgstr "<!empty>"
